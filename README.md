Hello :)

Some informations from me:

Browser used: Google Chrome
Webpack config: create-react-app (If it is neccessary to configure it by myself, just let me know)

Used tools:
- Ramda (Composing functions)
- Redux
- Normalizr (In order to normalize API response)
- Recompose (Again, to implement sweetnes of functional programming into react)

Things I'm aware of:

- Debounce should be implemented on search fields
- UI state partially is in both: component state and redux (In real life I would make every state stored in redux)
- State between lists is shared, which means that old state sometimes might "blink" when route is changed
- Shouldn't have been using global css selectors, but focused more on React/JS side than on styling
- Inline functions - There's some discussions around these, but as far it doesn't affect performance - it's ok (Just to tell that I'm aware of it)
- GIT repository is total mess. Got a lot of breaks doing this task and never remembered what actually I'm commiting due to large amount of files. Don't worry, usually it doesn't look like this :)

Sorry fot the amount of time it took (Way too long), but it's hard to work and do another things at once