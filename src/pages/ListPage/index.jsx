// @flow

import { Fragment } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { withState, compose, lifecycle } from 'recompose'
import { removeList, fetchLists } from 'actions/lists'
import Todos from 'containers/Todos'
import Search from 'containers/Search'
import FilterSwitch from 'containers/FilterSwitch'
import TopBar from 'components/TopBar'
import Button from 'components/Button'
import CreateTodo from 'components/CreateTodo'
import Modal from 'components/Modal'
import { getTodoList } from 'selectors/lists'

const toggleCreateModal = (state: Object): boolean => !state

const ListPage = ({ toggleModal, modalIsOpen, removeList, history, todolist }) => {

  // This could be replaced with HOC, but it's only one occurence of this.

  if (!todolist) {
    return 'Loading...'
  } 

  return (
    <Fragment>
      <TopBar title={todolist.name}>
        <Button 
          onClick={() => toggleModal(toggleCreateModal)} 
          className="btn btn-primary" 
          value="+ Add Todo" 
        />
        <Button
          onClick={() => {
            removeList(todolist.id).then(() => {
              history.push('/')
            })
          }}
          className="btn btn-danger"
          value="Remove list"
        />
      </TopBar>
      <div className="row">
        <div className="col-9">
          <Todos />
        </div>
        <div className="col-3">
          <Search searchType="todos" />
          <FilterSwitch />
        </div>
      </div>
      <Modal isOpen={modalIsOpen} toggle={toggleModal}>
        <CreateTodo />
      </Modal>
    </Fragment>
  )

}

const mapStateToProps = (state: Object, props: Object): Object => ({
  todolist: getTodoList(state, props.match.params.id)
}) 

export default compose(
  connect(mapStateToProps, { removeList, fetchLists }),
  lifecycle({
    componentDidMount() {
      this.props.fetchLists()
    }
  }),
  withRouter,
  withState('modalIsOpen', 'toggleModal', false)
)(ListPage)