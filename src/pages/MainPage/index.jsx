import { Fragment } from 'react' 
import { withState } from 'recompose'
import CreateList from 'components/CreateList'
import Modal from 'components/Modal'
import TodoLists from 'containers/TodoLists'
import Search from 'containers/Search'
import TopBar from 'components/TopBar'
import Button from 'components/Button'

const toggleCreateModal = (state) => !state

const MainPage = ({ toggleModal, modalIsOpen }) => (
  <Fragment>
    <TopBar>
      <Button 
        onClick={() => toggleModal(toggleCreateModal)} 
        className="btn btn-primary" 
        value="+ Add list" 
      />
    </TopBar>
    <Search searchType="lists" />
    <TodoLists />
    <Modal isOpen={modalIsOpen} toggle={toggleModal}>
      <CreateList />
    </Modal>
  </Fragment>
)

export default withState('modalIsOpen', 'toggleModal', false)(MainPage)