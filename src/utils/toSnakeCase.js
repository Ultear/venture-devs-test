// @flow

export default (phrase: string): string => {
  const upperChars = phrase.match(/([A-Z])/g);
  if (!upperChars) return phrase;

  let str = phrase.toString();
  for (let i = 0, n = upperChars.length; i < n; i++) {
      str = str.replace(new RegExp(upperChars[i]), '_' + upperChars[i].toLowerCase());
  }

  if (str.slice(0, 1) === '_') {
      str = str.slice(1);
  }

  return str;
};