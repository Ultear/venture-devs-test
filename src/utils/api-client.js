import agent from 'superagent'
import Promise from 'bluebird'

const APIUrl = 'https://todos.venturedevs.net/api'

const post = (endpoint, data = {}) => new Promise((resolve, reject) => {
  agent
    .post(`${APIUrl}${endpoint}`)
    .send(data)
    .end((err, res) => resolve(JSON.parse(res.text)))
})

const get = (endpoint, query = {}) => new Promise((resolve, reject) => {
  agent
    .get(`${APIUrl}${endpoint}`)
    .query(query)
    .end((err, res) => {
      const response = JSON.parse(res.text)

      if(err) {
        const error = new Error('Request error')
        reject(error)
      }

      resolve(response)
    })
})

const put = (endpoint, data = {}) => new Promise((resolve, reject) => {
  agent
    .put(`${APIUrl}${endpoint}`)
    .send(data)
    .end((err, res) => {
      const response = JSON.parse(res.text)

      if(err) {
        const error = new Error('Request error')
        reject(error)
      }

      resolve(response)
    })
})

const remove = (endpoint) => new Promise((resolve, reject) => {
  agent
    .delete(`${APIUrl}${endpoint}`)
    .end((err, res) => {
      if(err) {
        const error = new Error('Request error')
        reject(error)
      }
      resolve()
    })
})

export default {
  post,
  get,
  put,
  delete: remove,
}