import { normalize } from 'normalizr'
import { merge } from 'ramda'

export default ({ dispatch, getState }) => next => action => {
  const {types, callAPI, shouldCallAPI = () => true, schema, payload} = action
  if(!types) return next(action)

  if(!Array.isArray(types) || types.length !== 3 || !types.every(type => typeof type === 'string')){
    throw new Error('Expected an array of 3 string types.')
  }

  if(typeof callAPI !== 'function') {
    throw new Error('Expected callAPI to be a function.')
  }

  if(!shouldCallAPI(getState())) return

  const [requestType, successType, failureType] = types

  dispatch({ type: requestType })

  return new Promise((resolve, reject) => {
    callAPI().then(
      res => {
        const normalizedData = schema ? normalize(res, schema) : res

        dispatch({
          type: successType,
          payload: merge(payload, normalizedData)
        })
        resolve(payload)
      },
      err => {
        dispatch({
          type: failureType,
          errors: err,
        })
        reject({...err.bag})
      }
    )
  })
}