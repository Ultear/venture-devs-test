import { connect } from 'react-redux'
import { search } from 'actions/search'
import styles from './Search.module.scss'

const Search = ({ keyword, search, searchType }) => (
  <input 
    className={styles.Search} 
    type="text"
    value={keyword}
    onChange={(e) => search(searchType, e.target.value)}
    placeholder="Start typing to search" 
  />
)

const mapStateToProps = (state, props) => ({
  keyword: state.search[props.searchType]
})

export default connect(mapStateToProps, { search })(Search)