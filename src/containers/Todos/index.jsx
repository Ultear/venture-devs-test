import { connect } from 'react-redux'
import { lifecycle, compose } from 'recompose' 
import { map } from 'ramda'
import { withRouter } from 'react-router-dom'
import { fetchTodos, updateTodo, removeTodo } from 'actions/todos'
import { getTodos } from 'selectors/todos'
import TodoItem from 'components/TodoItem'

import styles from './Todos.module.scss'

const Todos = ({ todos = [], updateTodo, removeTodo }) => (
  <table className={styles.Todos}>
    <thead>
      <tr>
        <th></th>
        <th>Tasks</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      { map(todo => 
          <TodoItem 
            key={`todo_${todo.id}`} 
            remove={removeTodo} 
            update={updateTodo} 
            {...todo} 
          />, 
      todos) }
    </tbody>
  </table>
)

const mapStateToProps = (state, props) => ({
  todos: getTodos(state),
})

export default compose(
  connect(mapStateToProps, { fetchTodos, updateTodo, removeTodo }),
  withRouter,
  lifecycle({
    componentDidMount() {
      this.props.fetchTodos(this.props.match.params.id)
    }
  })
)(Todos)