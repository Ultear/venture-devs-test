import { connect } from 'react-redux'
import { SHOW_ALL, SHOW_ACTIVE, SHOW_COMPLETED } from 'actions/constants'
import { toggleFilter } from 'actions/filter'


const FilterSwitch = ({ toggleFilter }) => (
  <select onChange={(e) => toggleFilter(e.target.value)}>
    <option value={SHOW_ALL}>Show all</option>
    <option value={SHOW_ACTIVE}>Show active</option>
    <option value={SHOW_COMPLETED}>Show completed</option>
  </select>
)

export default connect(
  f => f,
  { toggleFilter }
)(FilterSwitch)