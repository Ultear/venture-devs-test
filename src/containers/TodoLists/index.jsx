// @flow

import { connect } from 'react-redux'
import { lifecycle, compose } from 'recompose'
import { fetchLists } from 'actions/lists'
import { getLists } from 'selectors/lists';
import TodoListItem from 'components/TodoListItem'
import styles from './TodoLists.module.scss'

type Props = {
  lists: Array<Object>,
}

const TodoLists = (props: Props) => (
  <ul className={styles.TodoLists}>
    { props.lists.map((list: Object, index: number) => <TodoListItem key={index} {...list} />) }
  </ul>
)

const mapStateToProps = (state: Object) => ({
  lists: getLists(state),
})

export default compose(
  connect(mapStateToProps, { fetchLists }),
  lifecycle({
    componentDidMount() {
      this.props.fetchLists()
    }
  })
)(TodoLists)