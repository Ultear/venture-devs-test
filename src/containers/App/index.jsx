import { Route } from 'react-router-dom'
import MainPage from 'pages/MainPage'
import ListPage from 'pages/ListPage'

const App = () => (
  <div className="container">
    <Route exact path="/" component={MainPage} />
    <Route path="/list/:id" component={ListPage} />
  </div>
)

export default App