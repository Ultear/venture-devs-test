const Button = ({value, ...props}) => (
  <button {...props}>
    { value }
  </button>
)

export default Button
