import { Link } from 'react-router-dom'

import styles from './TopBar.module.scss'

const TopBar = ({ children, title, onConfirm, editable = false }) => (
  <div className={styles.TopBar}>
    { title && <Link to="/">Back</Link> }
    <span className={styles.TopBarText}>{ title ? title : 'Todo lister' }</span>
    <div>
      { children }
    </div>
  </div>
)

export default TopBar