import { branch, renderComponent } from 'recompose'
import Input from './Input'

const CommonText = ({ value }) => <span>{value}</span> 

const EditableText = ({ onConfirm, value, isEditing = false }) => 
  branch(
    isEditing,
    renderComponent(() => <Input onConfirm={onConfirm} value={value} />),
    renderComponent(CommonText)
  )

export default EditableText