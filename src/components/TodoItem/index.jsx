import Button from 'components/Button'

const TodoItem = ({ name, is_complete, update, remove, id, todo_list }) => (
  <tr>
    <td>
      <input 
        checked={is_complete} 
        onChange={() => update(id, { is_complete: !is_complete, name, todo_list })} 
        type="checkbox" 
      />
    </td>
    <td
      style={{
        textDecoration: is_complete ? 'line-through' : 'none'
      }}
    >
      { name }
    </td>
    <td>
      <Button
        onClick={() => {
          const name = prompt('New task name')
          update(id, { is_complete: is_complete, name, todo_list })
        }}
        className="btn"
        value="Edit" 
      />
      <Button
        onClick={() => remove(id)}
        className="btn"
        value="Remove" 
      />
    </td>
  </tr>
)

export default TodoItem