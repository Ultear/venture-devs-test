import { Link } from 'react-router-dom'
import styles from './TodoListItem.module.scss'

const TodoListItem = ({ name, todos_count, id }) => 
  <li className={styles.TodoListItem}>
    <Link to={`/list/${id}`}>
      { name } 
      <span className={styles.CountBadge}>{ todos_count }</span>
    </Link>
  </li>

export default TodoListItem