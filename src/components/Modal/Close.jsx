import { CloseBtn } from './Modal.module.scss'

const Close = ({ onClick }) => <i className={CloseBtn} onClick={onClick} /> 

export default Close