import { cloneElement } from 'react'
import { createPortal } from 'react-dom'
import { renderNothing, branch, defaultProps, compose } from 'recompose'
import Close from './Close'
import styles from './Modal.module.scss'


const hideIfNotVisible = isOpen => branch(
  isOpen,
  renderNothing
)

const Modal = ({ children, closable, toggle, ...props}) => 
  createPortal(
    <div className={styles.Modal}>
      <div className={styles.ModalContent}>
        { closable && <Close onClick={() => toggle(false)} /> }
        { cloneElement(children, { toggle }) }
      </div>
    </div>,
    document.querySelector('#portal-root')
  )

export default compose(
  defaultProps({
    isOpen: false,
    closable: true,
    toggle: () => {},
  }),
  hideIfNotVisible(props => !props.isOpen)
)(Modal)
