import { connect } from 'react-redux'
import { compose } from 'recompose'
import { createTodo } from 'actions/todos'
import { withRouter } from 'react-router-dom'
import styles from './CreateTodo.module.scss'

const CreateTodoModal = ({ createTodo, toggle, match }) => {

  const onSubmit = (e) => {
    e.preventDefault()

    const data = {
      name: e.target.name.value,
      todo_list: match.params.id,
      is_complete: false,
    }

    createTodo(data)
    toggle(false)
  }

  return (
    <form onSubmit={onSubmit} className={styles.CreateListForm}>
      <h2>Add new task</h2>
      <div className={styles.SingleHorizontalForm}>
        <input type="text" name="name" />
        <input type="submit" value="Add" />
      </div>
    </form>
  )
}

export default compose(
  connect(f => f, { createTodo }),
  withRouter
)(CreateTodoModal)