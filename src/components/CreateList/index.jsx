import { connect } from 'react-redux'
import { createList } from 'actions/lists'
import styles from './CreateList.module.scss'

const CreateListModal = ({ createList, toggle }) => {

  const onSubmit = (e) => {
    e.preventDefault()
    const { name } = e.target
    createList(name.value)
    toggle(false)
  }

  return (
    <form onSubmit={onSubmit} className={styles.CreateListForm}>
      <h2>Add new list</h2>
      <div className={styles.SingleHorizontalForm}>
        <input type="text" name="name" />
        <input type="submit" value="Add" />
      </div>
    </form>
  )
}

export default connect(f => f, { createList })(CreateListModal)