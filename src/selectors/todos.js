// @flow

import { filter, map, pipe, prop } from 'ramda'
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from 'actions/constants'
import { search } from 'selectors/helpers'

const getVisibleTodos = (currentFilter: string) => (todos: Array<Object>) => {
  switch (currentFilter) {
    case SHOW_ALL:
      return todos
    case SHOW_COMPLETED:
      return filter(todo => todo.is_complete === true, todos)
    case SHOW_ACTIVE:
      return filter(todo => todo.is_complete === false, todos)
    default:
      return todos
  }
} 

export const getTodos = (state: Object) => 
  pipe(
    prop('allIds'),
    map(id => state.todos.byId[id]),
    search(state.search.todos),
    getVisibleTodos(state.filter)
  )(state.todos)
