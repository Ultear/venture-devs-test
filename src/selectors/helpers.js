// @flow

import { filter } from 'ramda'

export const search = (keyword: string) => filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()))