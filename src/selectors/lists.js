// @flow

import { map, pipe } from 'ramda'
import { search } from 'selectors/helpers'

export const getLists = (state: Object) => pipe(
  map(id => state.lists.byId[id]),
  search(state.search.lists)
)(state.lists.allIds)

export const getTodoList = (state: Object, id: number) => state.lists.byId[id]
