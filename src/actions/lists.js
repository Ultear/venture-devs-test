// @flow

import Api from 'utils/api-client'
import { 
  REQUEST_LISTS, 
  REQUEST_LISTS_SUCCESS, 
  REQUEST_LISTS_FAILURE,
  REQUEST_EDIT_LIST,
  REQUEST_EDIT_LIST_SUCCESS,
  REQUEST_EDIT_LIST_FAILURE,
  REMOVE_LIST,
  REMOVE_LIST_SUCCESS,
} from "./constants"

import { listsCollection, list } from './schema'

export const createList = (name: string): Object => ({
  types: [REQUEST_LISTS, REQUEST_LISTS_SUCCESS, REQUEST_LISTS_FAILURE],
  callAPI: (): Promise => Api.post('/todolists/', { name }),
  schema: list,
})

export const removeList = (id: number): Object => ({
  types: [REMOVE_LIST, REMOVE_LIST_SUCCESS, REMOVE_LIST_SUCCESS],
  callAPI: (): Promise => Api.delete(`/todolists/${id}`),
})

export const editList = (id: number, params: Object): Object => ({
  types: [REQUEST_EDIT_LIST, REQUEST_EDIT_LIST_SUCCESS, REQUEST_EDIT_LIST_FAILURE],
  callAPI: (): Promise => Api.put(`/todolists/${id}`, params),
  schema: list,
})

export const fetchLists = (): Object => ({
  types: [REQUEST_LISTS, REQUEST_LISTS_SUCCESS, REQUEST_LISTS_FAILURE],
  callAPI: (): Promise => Api.get('/todolists/'),
  schema: listsCollection,
})