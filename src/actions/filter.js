import { SET_FILTER } from 'actions/constants'

export const toggleFilter = (filter) => ({
  type: SET_FILTER,
  filter,
})