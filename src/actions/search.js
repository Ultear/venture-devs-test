// @flow

import { SEARCH } from "actions/constants"

export const search = (searchType: string, keyword: string): Object => ({
  type: SEARCH,
  searchType,
  keyword,
})