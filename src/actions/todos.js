// @flow

import Api from 'utils/api-client'
import { 
  REQUEST_TODOS, 
  REQUEST_TODOS_SUCCESS, 
  REQUEST_TODOS_FAILURE, 
  CREATE_TODO, 
  CREATE_TODO_SUCCESS, 
  CREATE_TODO_FAILURE,
  REMOVE_TODO, 
  REMOVE_TODO_SUCCESS, 
  REMOVE_TODO_FAILURE,
  TOGGLE_TODO,
  TOGGLE_TODO_SUCCESS,
  TOGGLE_TODO_FAILURE,
} from "./constants"

import { todosCollection, todo } from './schema'

export const createTodo = (params: Object) => ({
  types: [CREATE_TODO, CREATE_TODO_SUCCESS, CREATE_TODO_FAILURE],
  callAPI: (): Promise => Api.post('/todos/', params),
  schema: todo,
})

export const removeTodo = (id: number) => ({
  types: [REMOVE_TODO, REMOVE_TODO_SUCCESS, REMOVE_TODO_FAILURE],
  callAPI: (): Promise => Api.delete(`/todos/${id}`),
  payload: {
    id,
  }
})

export const updateTodo = (id: number, params: Object) => ({
  types: [TOGGLE_TODO, TOGGLE_TODO_SUCCESS, TOGGLE_TODO_FAILURE],
  callAPI: (): Promise => Api.put(`/todos/${id}/`, params),
  schema: todo,
})

export const fetchTodos = (id: number): Object => ({
  types: [REQUEST_TODOS, REQUEST_TODOS_SUCCESS, REQUEST_TODOS_FAILURE],
  callAPI: (): Promise => Api.get(`/todolists/${id}/`),
  schema: todosCollection,
  payload: {
    listId: id,
  }
})