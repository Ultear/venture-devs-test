import { schema } from 'normalizr'

export const todo = new schema.Entity('todos')
export const todosCollection = [ todo ]

export const list = new schema.Entity('lists')
export const listsCollection = [ list ]
