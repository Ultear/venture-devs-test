import { SEARCH } from "actions/constants"
import { combineReducers } from "redux";

const todos = (state = '', action) => {
  if (action.type === SEARCH && action.searchType === 'todos') {
    return action.keyword
  }

  return state
}

const lists = (state = '', action) => {
  if (action.type === SEARCH && action.searchType === 'lists') {
    return action.keyword
  }

  return state
}

export default combineReducers({
  todos,
  lists,
})