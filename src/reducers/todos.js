import { combineReducers } from 'redux'
import { merge, append, without } from 'ramda'
import { 
  REQUEST_TODOS, 
  REQUEST_TODOS_SUCCESS, 
  REQUEST_TODOS_FAILURE,
  CREATE_TODO_SUCCESS,
  REMOVE_TODO_SUCCESS,
} from "actions/constants"


const byId = (state = {}, action) => {
  if (!action.payload) return state
  const { entities } = action.payload

  if (!entities || !entities.hasOwnProperty('todos')) return state
  return merge(state, entities['todos'])
}

const allIds = (state = [], action) => {
  switch (action.type) {
    case REQUEST_TODOS_SUCCESS:
      return action.payload.result
    case CREATE_TODO_SUCCESS:
      return append(action.payload.result, state)
    case REMOVE_TODO_SUCCESS:
      return without([action.payload.id], state)
    default: 
      return state
  }
}

const isFetching = (state = false, action) => {
  switch (action.type) {
    case REQUEST_TODOS:
      return true
    case REQUEST_TODOS_SUCCESS:
    case REQUEST_TODOS_FAILURE:
      return false
    default:
      return state
  }
}

export default combineReducers({
  byId,
  allIds,
  isFetching,
})
