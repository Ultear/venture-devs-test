import { combineReducers } from 'redux'
import lists from './lists'
import todos from './todos'
import filter from './filter'
import search from './search'

export default combineReducers({ 
  lists,
  todos,
  filter,
  search,
})