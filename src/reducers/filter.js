import { SHOW_ALL, SET_FILTER } from 'actions/constants'

const reducer = (state = SHOW_ALL, action) => {

  if (action.type === SET_FILTER) {
    return action.filter
  }

  return state

}

export default reducer