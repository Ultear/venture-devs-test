import { combineReducers } from 'redux'
import { merge } from 'ramda'
import { 
  REQUEST_LISTS, 
  REQUEST_LISTS_SUCCESS, 
  REQUEST_LISTS_FAILURE,
} from "actions/constants"


const byId = (state = {}, action) => {
  if (!action.payload) return state
  const { entities } = action.payload

  if (!entities || !entities.hasOwnProperty('lists')) return state
  return merge(state, entities['lists'])
}

const allIds = (state = [], action) => {
  switch (action.type) {
    case REQUEST_LISTS_SUCCESS:
      const { result } = action.payload
      return Array.isArray(result) ? result : result
    default: 
      return state
  }
}

const isFetching = (state = false, action) => {
  switch (action.type) {
    case REQUEST_LISTS:
      return true
    case REQUEST_LISTS_SUCCESS:
    case REQUEST_LISTS_FAILURE:
      return false
    default:
      return state
  }
}

export default combineReducers({
  byId,
  allIds,
  isFetching
})
