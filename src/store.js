import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import APIUtil from 'utils/api-util'
import rootReducer from 'reducers'

const middlewares = [
  APIUtil,
  thunk,
]

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, {}, composeEnhancers(
  applyMiddleware(...middlewares)
  )
)

if (module.hot) {
  module.hot.accept('reducers', () => {
    const nextReducer = require('reducers').default
    store.replaceReducer(nextReducer)
  });
}

export default store